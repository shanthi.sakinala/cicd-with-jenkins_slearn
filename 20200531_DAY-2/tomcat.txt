ubuntu@ip-172-31-18-86:~$ sudo apt-get update
Ign:1 https://pkg.jenkins.io/debian-stable binary/ InRelease
Hit:2 https://pkg.jenkins.io/debian-stable binary/ Release                                
Hit:3 http://dl.google.com/linux/chrome/deb stable InRelease                                                   
Hit:4 http://ppa.launchpad.net/hermlnx/xrdp/ubuntu xenial InRelease                                            
Get:5 http://security.ubuntu.com/ubuntu xenial-security InRelease [109 kB]                     
Hit:6 http://ap-south-1.ec2.archive.ubuntu.com/ubuntu xenial InRelease              
Get:8 http://ap-south-1.ec2.archive.ubuntu.com/ubuntu xenial-updates InRelease [109 kB]
Get:9 http://ap-south-1.ec2.archive.ubuntu.com/ubuntu xenial-backports InRelease [107 kB]     
Fetched 325 kB in 1s (308 kB/s)   
Reading package lists... Done
ubuntu@ip-172-31-18-86:~$ 
ubuntu@ip-172-31-18-86:~$ java -version
java version "1.8.0_201"
Java(TM) SE Runtime Environment (build 1.8.0_201-b09)
Java HotSpot(TM) 64-Bit Server VM (build 25.201-b09, mixed mode)
ubuntu@ip-172-31-18-86:~$ 
ubuntu@ip-172-31-18-86:~$ echo $JAVA_HOME

ubuntu@ip-172-31-18-86:~$ source /etc/environment
ubuntu@ip-172-31-18-86:~$ echo $JAVA_HOME
/usr/lib/jvm/java-8-oracle/
ubuntu@ip-172-31-18-86:~$ cat /etc/environment
PATH="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games"

# Java Home Path Setup
JAVA_HOME="/usr/lib/jvm/java-8-oracle/"

MAVEN_HOME="/usr/share/maven"
M2_HOME=$MAVEN_HOME/bin
ubuntu@ip-172-31-18-86:~$



STEP-2 : Download, Install & Configure Tomcat on Lab :



 If you have not changed any configuration files, please examine the file conf/tomcat-users.xml in your installation. That file must contain the credentials to let you use this webapp.

For example, to add the manager-gui role to a user named tomcat with a password of s3cret, add the following to the config file listed above.

<role rolename="manager-gui"/>
<user username="tomcat" password="s3cret" roles="manager-gui"/>

Note that for Tomcat 7 onwards, the roles required to use the manager application were changed from the single manager role to the following four roles. You will need to assign the role(s) required for the functionality you wish to access.

    manager-gui - allows access to the HTML GUI and the status pages
    manager-script - allows access to the text interface and the status pages
    manager-jmx - allows access to the JMX proxy and the status pages
    manager-status - allows access to the status pages only

The HTML interface is protected against CSRF but the text and JMX interfaces are not. To maintain the CSRF protection:

    Users with the manager-gui role should not be granted either the manager-script or manager-jmx roles.
    If the text or jmx interfaces are accessed through a browser (e.g. for testing since these interfaces are intended for tools not humans) then the browser must be closed afterwards to terminate the session.

For more information - please see the Manager App How-To. 



 If you have not changed any configuration files, please examine the file conf/tomcat-users.xml in your installation. That file must contain the credentials to let you use this webapp.

For example, to add the admin-gui role to a user named tomcat with a password of s3cret, add the following to the config file listed above.

<role rolename="admin-gui"/>
<user username="tomcat" password="s3cret" roles="admin-gui"/>

Note that for Tomcat 7 onwards, the roles required to use the host manager application were changed from the single admin role to the following two roles. You will need to assign the role(s) required for the functionality you wish to access.

    admin-gui - allows access to the HTML GUI
    admin-script - allows access to the text interface

The HTML interface is protected against CSRF but the text interface is not. To maintain the CSRF protection:

    Users with the admin-gui role should not be granted the admin-script role.
    If the text interface is accessed through a browser (e.g. for testing since this interface is intended for tools not humans) then the browser must be closed afterwards to terminate the session.

manager-gui - allows access to the HTML GUI and the status pages
    manager-script - allows access to the text interface and the status pages
    manager-jmx - allows access to the JMX proxy and the status pages
    manager-status - allows access to the status pages only

<role rolename="admin-gui"/>
<role rolename="admin-script"/>
<role rolename="manager-gui"/>
<role rolename="manager-script"/>
<role rolename="manager-jmx"/>
<role rolename="manager-status"/>
<user username="admin" password="redhat@123" roles="admin-gui,admin-script,manager-gui,manager-script,manager-jmx,manager-status"/>

https://websiteforstudents.com/how-to-install-jfrog-artifactory-on-ubuntu-18-04-16-04/




First install latest version Jfrog

Copy Lsb-release setting under usr/share/maven/config/setting.xml

Enter Username and Password hardcoded in setting.xml

Second made changes in Configure System

Third under Configure Global Security --> 

CSRF Protection --> Click on checkbox --> Enable proxy compatibility

Build Step -->  Root Pom as Pom.xml and Goal as deploy

Add Post build Action : Deploy artifacts to Artifactory

Save 

And Build Now